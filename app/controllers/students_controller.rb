#from ch12_march24
class StudentsController < InheritedResources::Base

  private

    def student_params
      params.require(:student).permit(:country, :fio, :lastname, :firstname, :email, :comment, :user_id)
    end
end
