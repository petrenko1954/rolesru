class UsersController < ApplicationController
# libr Листинг :9.5 :9.28: :9.33 Листинг 9.22: Предфильтр correct_user для защиты страниц edit/update. ЗЕЛЕНЫЙ app/controllers/users_controller.rb Листинг 9.27: 9.28 Листинг 9.32: Требование входа пользователя для действия index. ЗЕЛЕНЫЙ app/controllers/users_controller.rb :9.53 :Sept14 Листинг 12.25: Действия following и followers. app/controllers/users_controller.rb 
 #before_action :authenticate_user!
 #March08  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
                                        :following, :followers]
 
 before_action :correct_user,   only: [:edit, :update]   
  before_action :admin_user,     only: :destroy




#Листинг 9.54: Предфильтр, ограничивающий доступ к действию destroy. app/controllers/users_controller.rb  :11.22
 
 before_action :admin_user,     only: :destroy
#Листинг 11.22: Добавление переменной экземпляра @microposts к действию show. app/controllers/users_controller.rb 
def show
  @user = User.find(params[:id])
      #@articles = Article.includes(:article_files, :author).all
      #@conferences = current_user.conferences
@microposts = @user.microposts.paginate(page: params[:page])
#@students = @user.students.paginate(page: params[:page])
#debugger   
@student  = current_user.students.build if logged_in?
 
end
         #Листинг 7.12: 
def new
         #@students = @user.students.paginate(page: params[:page])
         #@student = Student.new
         @user = User.new
end

#Листинг 9.32: Требование входа пользователя для действия index. ЗЕЛЕНЫЙ app/controllers/users_controller.rb   

def index   #users Листинг 9.5 - :9.33
     #@users = User.all
#Листинг 9.42:
 @users = User.paginate(page: params[:page])  
end

     #Листинг :7.1  :7.17 # 7.23 7.24 8.22
     #:7.16  
     #Листинг 9.1: Действие edit.s

     #def create
     # @owner = Owner.new(params[:owner])
def create
#@client = Client.new(params[:client])
    
#@user = User.new(params[:user])

#@comment = @article.comments.create(comment_params)
#Aug_29    @student = @user.students.create(student_params)

@user = User.new(user_params)
    if @user.save
      log_in @user
               flash[:success] = "User registered, Welcome to the roles -b formwithSept17-master!"
          redirect_to @user
#redirect_to static_pages_home

       else
      render 'new'
        end
  end

#Листинг 9.53: Добавление работающего действия destroy. app/controllers/users_controller.rb 
 def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

 #Листинг 9.1: 
def edit
    @user = User.find(params[:id])
#@student = @user.students

#@student = student.user.find(params[:id])
  end

 # Подтверждает администратора.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

#Листинг 9.5: Первоначальное действие update. app/controllers/users_controller.rb 
def update

#user.update!(user_params)



    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated !"
     redirect_to @user
#redirect_to your_controller_action_url
#redirect_to static_pages_home


    else
      render 'edit'
    end
       end

def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end


                   private

    def user_params
    params.require(:user).permit!
#.permit(:name, :email, :password, :password_confirmation, students_attributes: [:fio]) 

     end


 #def student_params
#params.require(:user.student).permit(:fio) #, :email, :password,
  #  end
# Предфильтры
#Листинг 9.28: # Подтверждает вход пользователя
    def logged_in_user
      unless logged_in?
       store_location
       flash[:danger] = "Please log in."
       redirect_to login_url
            end
          end
# Подтверждает правильного пользователя
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
# Подтверждает администратора.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
 end
